variable "ssh_user" {sensitive=true}
variable "ssh_host" {sensitive=true}
variable "ssh_key" {sensitive=true}
variable "password" {sensitive=true}

module "docker_install" {
    source = "./modules/docker_install"
    ssh_host = var.ssh_host
    ssh_user = var.ssh_user
    ssh_key = var.ssh_key
    password = var.password
}

terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}

provider "docker" {
  host     = "ssh://${var.ssh_user}@${var.ssh_host}:22"
  ssh_opts = ["-o", "StrictHostKeyChecking=no", "-o", "UserKnownHostsFile=/dev/null", "-i", "${var.ssh_key}"]
}

resource "docker_image" "nginx" {
  name         = "nginx:latest"
  keep_locally = false
}

resource "docker_container" "nginx" {
  image = docker_image.nginx.image_id  
  name  = "tutorial"
  ports {
    internal = 80
    external = 8000
  }
}